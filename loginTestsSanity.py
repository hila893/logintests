import logging
import time

from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from webdriver_manager.chrome import ChromeDriverManager

LOGIN_PAGE_URL = "https://profile.w3schools.com/"
VALID_USER = 'ofirmb29@gmail.com'
VALID_PASS = '12Hila0189!'
LOGGED_IN_URL = "https://my-learning.w3schools.com/"
WRONG_PASS_ALERT = "Make sure you type your email and password correctly. " \
                         "Both your password and email are case-sensitive."
EMAIL_NOT_EXIST = "A user with this email does not exist"
SPACE_IN_CREDENTIALS = "Looks like you forgot something"

class loginTestsSanity():
    def __init__(self, user_name, password):
        self.username = user_name
        self.password = password

    def setup(self):
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        self.driver.get(LOGIN_PAGE_URL)

    def loginPositiveTest(self):
        self.setup()
        self.login(self.username, self.password)
        time.sleep(8)
        self.verify_user_logged_in()

    def login(self, username, password):
        self.driver.find_element(By.ID, "modalusername").clear()
        self.driver.find_element(By.ID, "current-password").clear()
        self.driver.find_element(By.ID, "modalusername").send_keys(username)
        self.driver.find_element(By.ID, "current-password").send_keys(password)
        self.driver.find_element(By.XPATH, "//span[text()='Log in']").click()

    def verify_user_logged_in(self):
        check_current_url = True if self.driver.current_url == LOGGED_IN_URL else False
        if check_current_url and self.check_logged_in_title() and self.check_if_logout_exist():
            logging.info("Log in work successfully")
            return
        logging.error("Login failed")
        print("Login Failed")
        raise

    def check_logged_in_title(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH,
                                                        "//h2[contains(text(), 'Hi,')]")))
            return True
        except NoSuchElementException:
            logging.error("'Hi, {username}' title is missing")
            raise

    def check_if_logout_exist(self):
        """ Check if logout button exist"""
        try:
            self.driver.find_element(By.XPATH, "//div[text()='Log out']")
            return True
        except NoSuchElementException:
            logging.error("Log out button is missing")
            raise

    def loginNegativTests(self):
        msg_xpath= "//div[text()='%s']"
        self.setup()
        self.login(self.username, "worngPass123!")
        self.verify_error_msg(msg_xpath % WRONG_PASS_ALERT)

        self.login("hila+102@gmail.com", self.password)
        self.verify_error_msg(msg_xpath % EMAIL_NOT_EXIST)

        self.login("hila+102@gmail.com", "jkhfsdd")
        self.verify_error_msg(msg_xpath % EMAIL_NOT_EXIST)

        self.login("test space", "      ")
        msg_xpath = "//span[text()='%s']"
        self.verify_error_msg(msg_xpath % SPACE_IN_CREDENTIALS)

    def verify_error_msg(self, element, selector_type=By.XPATH):
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((selector_type, element)))
            logging.info("User with wrong credential, didn't logged in.")
            print("User with wrong credential, didn't logged in.")
            return
        except NoSuchElementException as e:
            logging.error("User with wrong credential, logged in.")
            raise e

    def test_hidden_and_show_password(self, password):
        self.setup()
        self.driver.find_element(By.ID, "current-password").clear()
        self.driver.find_element(By.ID, "current-password").send_keys(password)
        self.test_hide_button()
        self.driver.find_element(By.XPATH, "//span[contains(@class, 'PasswordInput_show')]").click()
        self.test_show_button(password)
        self.driver.find_element(By.XPATH, "//span[contains(@class, 'PasswordInput_show')]").click()
        self.test_hide_button()

    def test_hide_button(self):
        try:
            if self.driver.find_element(By.ID, "current-password").get_attribute("type") == "password":
                logging.info("Password is hidden as default")
                print("Password is hidden as default")
        except NoSuchElementException:
            logging.error("Password is not hidden by default")
            raise

    def test_show_button(self, password):
        try:
            if self.driver.find_element(By.ID, "current-password").get_attribute('value') == password:
                logging.info("password is shown after click on 'Show' button")
                print("password is shown after click on 'Show' button")
        except NoSuchElementException:
            logging.error("Password is hidden after click on 'Show' button")
            raise

    def test_sign_up_button(self):
        self.driver.find_element(By.XPATH, "//span[text()=' Sign up']").click()
        try:
            self.driver.find_element(By.XPATH, "//h1[text()='Sign up']")
            logging.info("Sign up page opend successfully")
            print("Sign up page opend successfully")
        except NoSuchElementException:
            logging.error("Sign up page didn't open")

    def test_forgot_password(self):
        self.driver.find_element(By.LINK_TEXT, "Forgot password?").click()
        if self.driver.current_url == "https://profile.w3schools.com/reset":
            try:
                self.driver.find_element(By.XPATH, "//h1[text()='Reset your password']")
                logging.info("Reset page loaded successfully")
                print("Reset page loaded successfully")
            except NoSuchElementException:
                logging.error("Reset page failed to load")
                raise

    def test_login_by_external_web(self, web_name):
        self.setup()
        element = f"//div[text()='Continue with {web_name}']"
        body = self.driver.find_element(By.CSS_SELECTOR, "body")
        body.send_keys(Keys.PAGE_DOWN)
        time.sleep(2)
        self.driver.find_element(By.XPATH, element).click()
        if f"{web_name.lower()}.com" in self.driver.current_url:
            logging.info(f"Redirect to {web_name} login successfully")
            print(f"Redirect to {web_name} login successfully")
            return
        logging.error(f"Redirect to {web_name} login failed")
        raise

    def cleaunp(self):
        self.driver.close()

login_test = loginTestsSanity(VALID_USER, VALID_PASS)
login_test.loginNegativTests()
login_test.cleaunp()
login_test.loginPositiveTest()
login_test.cleaunp()
login_test.test_hidden_and_show_password(login_test.password)
login_test.cleaunp()
login_test.test_login_by_external_web("Google")
login_test.cleaunp()
login_test.test_login_by_external_web("Facebook")
login_test.cleaunp()
login_test.test_login_by_external_web("GitHub")
login_test.cleaunp()
